/* This file is a part of sqlitecpp library.
 Copyright © 2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN connection WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <string.h>
#include "sqlitecpp.h"

#if UNIT_TEST
// C++ Micro Unit Test Asset (CMUTA)
// https://evgenijvidershpan@bitbucket.org/evgenijvidershpan/cmuta.git
#include <cmuta.h>
#ifdef SELF_TEST
int main(){ return 0; }
#endif
#endif

namespace sqlite
{

#if UNIT_TEST
UNIT_TEST_BEGIN(monolite_reserve)
monolite first;
UNIT_TEST_CHECK(first.size() == 0);
UNIT_TEST_ASSERT(first.reserve(256, 1024*65));
UNIT_TEST_CHECK(first.get(0) == 0);
{
    monolite second = first;
    first.append(0, 0);
    first.append("first", sizeof("first"));
    UNIT_TEST_CHECK(second.size() == 2);
    second.append("second", sizeof("second"));
    UNIT_TEST_CHECK(first.size() == 3);
}
UNIT_TEST_CHECK(first.size() == 3);
UNIT_TEST_ASSERT(first.get(2)!= 0);
UNIT_TEST_CHECK(strcmp((const char*)first.get(2), "second") == 0);
UNIT_TEST_END(monolite_reserve)
#endif

bool monolite::reserve(size_t count, size_t size)
{
    detach();
    if (!count)
        return false;
    size += sizeof(shared_data) + sizeof(size_t) * (count-1);
    char* mem = new char[size+2]; // в конце буфера будет unicode '\0'
    mem[size] = 0;
    mem[size+1] = 0;
    shared = (shared_data*) mem;

    if (!shared)
        return false;

    shared->refs = 1;
    shared->size = size;
    shared->count = count;
    shared->saved = 0;
    return true;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(monolite_append)
monolite first;
UNIT_TEST_ASSERT(first.reserve(1, 0)); // 1 element with 0 byte buffer
UNIT_TEST_CHECK(first.append(0, 0));
UNIT_TEST_CHECK(first.append("", 1) == false);
UNIT_TEST_CHECK(first.get(0) == 0);
UNIT_TEST_CHECK(first.size() == 1);
UNIT_TEST_ASSERT(first.reserve(2, 1)); // 2 element with 1 byte buff
UNIT_TEST_CHECK(first.append(0, 0));
UNIT_TEST_CHECK(first.append("!", 1));
UNIT_TEST_CHECK(first.get(1) != 0);
UNIT_TEST_CHECK(strcmp((const char*)first.get(1), "!") == 0);
UNIT_TEST_END(monolite_append)
#endif

bool monolite::append(const void* pdata, size_t size)
{
    if (!shared)
        return false;
    // сколько строк было записано?
    size_t pos = shared->saved;
    if (pos >= shared->count)
        return false;

    shared->offsets[pos] = size; // может быть 0!
    char* dst = (char*)(&(shared->offsets[shared->count]));
    if (pos > 0) // компенсируем указатели
    {
        // предыдущая запись сохранила нам позицию
        size_t last = shared->offsets[pos-1];
        dst += last;
        // корректируем позицию следующей записи
        shared->offsets[pos] += last;
    }
    // проверяем хватит ли места в буфере.
    char* end = ((char*) shared) + shared->size;
    if ((dst + size) > end)
        return false;
    const char* str = (const char*) pdata;
    for (size_t i=0; i<size; ++i)
        dst[i] = str[i];
    shared->saved += 1;
    return true;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(monolite_get)
monolite first;
UNIT_TEST_CHECK(first.get(0) == 0);
UNIT_TEST_CHECK(first.get(1) == 0);
UNIT_TEST_CHECK(first.get(-1) == 0);
UNIT_TEST_ASSERT(first.reserve(1024, 1024*2));
char buf[] = "0";
for (int i=0; i<1024; ++i){
    buf[0] = '0'+(i % 10);
    first.append(buf, 2);
}
UNIT_TEST_CHECK(first.size() == 1024);
bool CheckValues = true;
for (int i=0; i<1024; ++i){
    const char* str = (const char*)first.get(i);
    if (!str || *str != '0'+(i % 10))
        CheckValues = false;
}
UNIT_TEST_CHECK(CheckValues == true);
UNIT_TEST_END(monolite_get)
#endif

const void* monolite::get(size_t nItem, size_t* lpSize) const
{
    if (!shared || nItem >= shared->saved)
        return 0;
    char* src = (char*)(&(shared->offsets[shared->count]));
    size_t next = shared->offsets[nItem];
    if (nItem > 0) // компенсируем указатели
    {
        size_t prev = shared->offsets[nItem-1];
        if (lpSize)
            *lpSize = next - prev;
        if (next == prev)
            return 0; // return ((char*)shared) + shared->size;
        return src + shared->offsets[nItem-1];
    }
    if (lpSize)
        *lpSize = next;
    return next? src : 0; // ((char*)shared) + shared->size;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(monolite_deflate)
monolite test;
UNIT_TEST_ASSERT(test.reserve(2048, 1024*1024));
UNIT_TEST_CHECK(test.reserved() == 1024*1024);
UNIT_TEST_CHECK(test.append("test str a", sizeof("test str a")));
UNIT_TEST_CHECK(test.append("test str b", sizeof("test str b")));
UNIT_TEST_CHECK(test.append("test str c", sizeof("test str c")));
test = test.deflate();
UNIT_TEST_CHECK(test.size() == 3);
UNIT_TEST_CHECK(test.reserved() == sizeof("test str a") * 3);
const char* str = (const char*)test.get(0);
UNIT_TEST_ASSERT(str != 0);
UNIT_TEST_CHECK(strcmp(str, "test str a") == 0);
str = (const char*) test.get(1);
UNIT_TEST_ASSERT(str != 0);
UNIT_TEST_CHECK(strcmp(str, "test str b") == 0);
str = (const char*) test.get(2);
UNIT_TEST_ASSERT(str != 0);
UNIT_TEST_CHECK(strcmp(str, "test str c") == 0);
UNIT_TEST_END(monolite_deflate)
#endif

monolite monolite::deflate()
{
    monolite rVal;
    if (!shared)
        return rVal;
    size_t cnt = shared->saved;
    if (cnt < 1)
        return rVal;
    size_t bytes = shared->offsets[shared->saved-1];
    if (rVal.reserve(cnt, bytes))
    {
        for (size_t i=0; i<cnt; ++i)
        {
            size_t len = 0;
            const void* pt = get(i, &len);
            rVal.append(pt, len);
        }
    }
    return rVal;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(monolite_inflate)
monolite test;
test = test.inflate(false);
UNIT_TEST_CHECK(test.size() == 0);
UNIT_TEST_ASSERT(test.reserve(1,0));
test = test.inflate(false);
UNIT_TEST_CHECK(test.append(0,0));
UNIT_TEST_CHECK(test.append(0,0));
UNIT_TEST_ASSERT(test.reserve(1024, 0xffff));
test = test.inflate(true);
UNIT_TEST_CHECK(test.append(0,0));
UNIT_TEST_END(monolite_inflate)
#endif

monolite monolite::inflate(bool onlyData)
{
    monolite rVal;
    if (!shared)
        return rVal;
    size_t cnt = onlyData? shared->count : shared->count * 2;
    if (cnt < 1)
        return rVal;
    // байты данных
    size_t dataBytes = shared->offsets[shared->saved-1] * 2;
    if (!dataBytes)
    {
        rVal.reserve(cnt, dataBytes);
        return rVal;
    }
    // служебные байты
    size_t srvBytes = sizeof(shared_data)+sizeof(size_t)*(cnt-1)+2;
    size_t newBytes = ((dataBytes + 0xFFFF) & (~(size_t)0xFFFF)) - srvBytes;

    if (rVal.reserve(cnt, newBytes))
    {
        for (int i=0,c=shared->count; i<c; ++i)
        {
            size_t len = 0;
            const void* pt = get(i, &len);
            rVal.append(pt, len);
        }
    }
    return rVal;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(monolite_reserved)
monolite first;
UNIT_TEST_CHECK(first.reserved() == 0);
UNIT_TEST_ASSERT(first.reserve(1024, 1024*2));
UNIT_TEST_CHECK(first.reserved() == 1024*2);
UNIT_TEST_ASSERT(first.reserve(1024, 1));
UNIT_TEST_CHECK(first.reserved() == 1);
UNIT_TEST_ASSERT(first.reserve(1, 1024));
UNIT_TEST_CHECK(first.reserved() == 1024);
UNIT_TEST_END(monolite_reserved)
#endif

size_t monolite::reserved() const
{
    if (!shared)
        return 0;
    return shared->size - (sizeof(shared_data) +
                           sizeof(size_t)*(shared->count-1));
};

#if UNIT_TEST
UNIT_TEST_BEGIN(monolite_summary)
monolite mnl;
UNIT_TEST_ASSERT(mnl.reserve(15, 8*2));
UNIT_TEST_ASSERT(mnl.append("monolit", 8));
UNIT_TEST_ASSERT(mnl.append("testing", 8));
UNIT_TEST_ASSERT(mnl.append(0, 0));
UNIT_TEST_ASSERT(mnl.append("", 0));
UNIT_TEST_CHECK(mnl.append("", 1) == false); // overflow test
UNIT_TEST_CHECK(strcmp((const char*)mnl.get(0), "monolit") == 0);
UNIT_TEST_CHECK(strcmp((const char*)mnl.get(1), "testing") == 0);
UNIT_TEST_CHECK(mnl.size() == 4);
monolite other = mnl;
UNIT_TEST_ASSERT(mnl.reserve(15, 8*2));
UNIT_TEST_CHECK(strcmp((const char*)other.get(1), "testing") == 0);
UNIT_TEST_END(monolite_summary)
#endif

connection::connection()
{
    pConnection = 0;
}

connection::~connection()
{
    if (pConnection)
        close();
}

int connection::open(const char *filename)
{
    int rc = close();
    if (rc != SQLITE_OK)
        return rc;
    return sqlite3_open(filename, &pConnection);
}

int connection::open16(const void *filename)
{
    int rc = close();
    if (rc != SQLITE_OK)
        return rc;
    return sqlite3_open16(filename, &pConnection);
}

int connection::close()
{
    if (!pConnection)
        return SQLITE_OK;
    int rc = sqlite3_close(pConnection);
    if (rc == SQLITE_OK)
        pConnection = 0;
    return rc;
}

int connection::exec(const char* szSql)
{
    if (!pConnection)
        return SQLITE_ERROR;
    return sqlite3_exec(pConnection, szSql, 0, 0, 0);
}

int connection::get_autocommit()
{
    if (!pConnection)
        return SQLITE_ERROR;
    return sqlite3_get_autocommit(pConnection);
}

#if UNIT_TEST
UNIT_TEST_BEGIN(connection_table_exists)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
UNIT_TEST_ASSERT(db.exec("CREATE TABLE ut_ctext(id INTEGER)") == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("ut_ctext"));
UNIT_TEST_ASSERT(db.exec("DROP TABLE ut_ctext") == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("ut_ctext") == false);
UNIT_TEST_END(connection_table_exists)
#endif

bool connection::table_exists(const char* szTable)
{
    if (!pConnection)
        return false;

    statement stmt;
    if (SQLITE_OK != stmt.prepare(
        "SELECT COUNT(*) FROM SQLITE_MASTER WHERE TYPE='table' AND NAME=?",
        -1, pConnection))
    {
        // trow... log... etc.
        return false;
    }
    if (SQLITE_OK != stmt.bind_text(1, szTable, -1, SQLITE_STATIC))
    {
        // trow... log... etc.
        return false;
    }
    if (stmt.step() != SQLITE_ROW)
    {
        // trow... log... etc.
        return false;
    }
    return stmt.column_int(0) > 0;
}

statement::statement()
{
    pStatement = 0;
}

statement::~statement()
{
    if (finalize() != SQLITE_OK)
    {
        // log... trow... etc.
    }
}

#if UNIT_TEST
UNIT_TEST_BEGIN(statement_prepare)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
statement stmt;
UNIT_TEST_ASSERT(stmt.prepare("CREATE TABLE ut_stmt_prepare(id INTEGER)",
                              -1, db) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.step() == SQLITE_DONE);
UNIT_TEST_CHECK(db.table_exists("ut_stmt_prepare"));
UNIT_TEST_ASSERT(db.exec("DROP TABLE ut_stmt_prepare") == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("ut_stmt_prepare") == false);
UNIT_TEST_ASSERT(stmt.prepare16(L"CREATE TABLE ut_stmt_prepare(id INTEGER)")
                 == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.step() == SQLITE_DONE);
UNIT_TEST_CHECK(db.table_exists("ut_stmt_prepare"));
UNIT_TEST_ASSERT(db.exec("DROP TABLE ut_stmt_prepare") == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("ut_stmt_prepare") == false);
UNIT_TEST_END(statement_prepare)
#endif

int statement::prepare(const char *szSql, int nBytes, sqlite3* lpConn)
{
    if (!lpConn)
        lpConn = db_handle();
    if (finalize() != SQLITE_OK)
    {
        // log... trow... etc.
    }
    if (!lpConn || !szSql)
        return SQLITE_ERROR;
    pStatement = 0;
    return sqlite3_prepare_v2(lpConn, szSql, nBytes, &pStatement, 0);
}

int statement::prepare16(const void *szSql, int nBytes, sqlite3* lpConn)
{
    if (!lpConn)
        lpConn = db_handle();
    if (finalize() != SQLITE_OK)
    {
        // log... trow... etc.
    }
    if (!lpConn || !szSql)
        return SQLITE_ERROR;
    pStatement = 0;
    return sqlite3_prepare16_v2(lpConn, szSql, nBytes, &pStatement, 0);
}

int statement::finalize()
{
    if (pStatement)
    {
        int rc = sqlite3_finalize(pStatement);
        if (rc != SQLITE_OK)
            return rc;
        pStatement = 0;
    }
    return SQLITE_OK;
}

int statement::reset()
{
    if (pStatement)
        return sqlite3_reset(pStatement);
    return SQLITE_ERROR;
}

int statement::step()
{
    if (pStatement)
        return sqlite3_step(pStatement);
    return SQLITE_ERROR;
}

sqlite3* statement::db_handle()
{
    if (pStatement)
        return sqlite3_db_handle(pStatement);
    return 0;
}

int statement::bind_blob(int nCol, const void* value, int nBytes, destructor dalc)
{
    return sqlite3_bind_blob(pStatement, nCol, value, nBytes, dalc);
}

int statement::bind_double(int nCol, double value)
{
    return sqlite3_bind_double(pStatement, nCol, value);
}

int statement::bind_int(int nCol, int value)
{
    return sqlite3_bind_int(pStatement, nCol, value);
}

int statement::bind_int64(int nCol, sqlite_int64 value)
{
    return sqlite3_bind_int64(pStatement, nCol, value);
}

int statement::bind_null(int nCol)
{
    return sqlite3_bind_null(pStatement, nCol);
}

int statement::bind_parameter_count()
{
    return sqlite3_bind_parameter_count(pStatement);
}

int statement::bind_parameter_index(const char* szName)
{
    return sqlite3_bind_parameter_index(pStatement, szName);
}

const char* statement::bind_parameter_name(int nCol)
{
    return sqlite3_bind_parameter_name(pStatement, nCol);
}

int statement::bind_text(int nCol, const char* value, int nBytes, destructor dalc)
{
    return sqlite3_bind_text(pStatement, nCol, value, nBytes, dalc);
}

int statement::bind_text16(int nCol, const void* value, int nBytes, destructor dalc)
{
    return sqlite3_bind_text16(pStatement, nCol, value, nBytes, dalc);
}

int statement::bind_value(int nCol, const sqlite3_value* value)
{
    return sqlite3_bind_value(pStatement, nCol, value);
}

int statement::clear_bindings()
{
    return sqlite3_clear_bindings(pStatement);
}

const char* statement::column_database_name(int nCol)
{
#ifdef SQLITE_ENABLE_COLUMN_METADATA
    return sqlite3_column_database_name(pStatement, nCol);
#else
    return 0;
#endif // SQLITE_ENABLE_COLUMN_METADATA
}

const char* statement::column_decltype(int nCol)
{
    return sqlite3_column_decltype(pStatement, nCol);
}

const char* statement::column_name(int nCol)
{
    return sqlite3_column_name(pStatement, nCol);
}

const char* statement::column_origin_name(int nCol)
{
#ifdef SQLITE_ENABLE_COLUMN_METADATA
    return sqlite3_column_origin_name(pStatement, nCol);
#else
    return 0;
#endif // SQLITE_ENABLE_COLUMN_METADATA
}

const char* statement::column_table_name(int nCol)
{
#ifdef SQLITE_ENABLE_COLUMN_METADATA
    return sqlite3_column_table_name(pStatement, nCol);
#else
    return 0;
#endif // SQLITE_ENABLE_COLUMN_METADATA
}

const unsigned char* statement::column_text(int nCol)
{
    return sqlite3_column_text(pStatement, nCol);
}

const void* statement::column_blob(int nCol)
{
    return sqlite3_column_blob(pStatement, nCol);
}

const void* statement::column_database_name16(int nCol)
{
#ifdef SQLITE_ENABLE_COLUMN_METADATA
    return sqlite3_column_database_name16(pStatement, nCol);
#else
    return 0;
#endif // SQLITE_ENABLE_COLUMN_METADATA
}

const void* statement::column_decltype16(int nCol)
{
    return sqlite3_column_decltype16(pStatement, nCol);
}

const void* statement::column_name16(int nCol)
{
    return sqlite3_column_name16(pStatement, nCol);
}

const void* statement::column_origin_name16(int nCol)
{
#ifdef SQLITE_ENABLE_COLUMN_METADATA
    return sqlite3_column_origin_name16(pStatement, nCol);
#else
    return 0;
#endif // SQLITE_ENABLE_COLUMN_METADATA
}

const void* statement::column_table_name16(int nCol)
{
#ifdef SQLITE_ENABLE_COLUMN_METADATA
    return sqlite3_column_table_name16(pStatement, nCol);
#else
    return 0;
#endif // SQLITE_ENABLE_COLUMN_METADATA
}

const void* statement::column_text16(int nCol)
{
    return sqlite3_column_text16(pStatement, nCol);
}

double statement::column_double(int nCol)
{
    return sqlite3_column_double(pStatement, nCol);
}

int statement::column_bytes(int nCol)
{
    return sqlite3_column_bytes(pStatement, nCol);
}

int statement::column_bytes16(int nCol)
{
    return sqlite3_column_bytes16(pStatement, nCol);
}

int statement::column_count()
{
    return sqlite3_column_count(pStatement);
}

int statement::column_int(int nCol)
{
    return sqlite3_column_int(pStatement, nCol);
}

int statement::column_type(int nCol)
{
    return sqlite3_column_type(pStatement, nCol);
}

sqlite_int64 statement::column_int64(int nCol)
{
    return sqlite3_column_int64(pStatement, nCol);
}

sqlite3_value* statement::column_value(int nCol)
{
    return sqlite3_column_value(pStatement, nCol);
}

#if UNIT_TEST
UNIT_TEST_BEGIN(statement_column_names)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
statement stmt;
UNIT_TEST_ASSERT(stmt.prepare("CREATE TABLE ut_stmt_cn"
    "(id INTEGER, content TEXT, title TEXT)", -1, db) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.step() == SQLITE_DONE);
UNIT_TEST_CHECK(db.table_exists("ut_stmt_cn"));
UNIT_TEST_ASSERT(stmt.prepare("SELECT * FROM ut_stmt_cn", -1, db) == SQLITE_OK);
monolite cols = stmt.column_names();
UNIT_TEST_CHECK(cols.size() == 3);
UNIT_TEST_ASSERT(cols.get(0) != 0);
UNIT_TEST_CHECK(strcmp((const char*)cols.get(0), "id") == 0);
UNIT_TEST_ASSERT(cols.get(1) != 0);
UNIT_TEST_CHECK(strcmp((const char*)cols.get(1), "content") == 0);
UNIT_TEST_ASSERT(cols.get(2) != 0);
UNIT_TEST_CHECK(strcmp((const char*)cols.get(2), "title") == 0);
UNIT_TEST_END(statement_column_names)
#endif

monolite statement::column_names()
{
    monolite rVal;
    int cols = column_count();
    if (cols < 1)
        return rVal;
    size_t nBytes = 0;
    for (int i=0; i<cols; ++i)
    {
        const char* str = column_name(i);
        size_t len = 0;
        if (str){
            const char* tmp = str; do len += 1; while (*tmp++);
        }
        nBytes += len;
    }
    if (!rVal.reserve(cols, nBytes))
        return rVal;
    for (int i=0; i<cols; ++i)
    {
        const char* str = column_name(i);
        size_t len = 0;
        if (str){
            const char* tmp = str; do len += 1; while (*tmp++);
        }
        rVal.append(str, len);
    }
    return rVal;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(statement_column_names16)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
statement stmt;
UNIT_TEST_ASSERT(stmt.prepare16(L"CREATE TABLE ut_stmt_cn16"
    L"(id INTEGER, content TEXT, title TEXT)", -1, db) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.step() == SQLITE_DONE);
UNIT_TEST_CHECK(db.table_exists("ut_stmt_cn16"));
UNIT_TEST_ASSERT(stmt.prepare16(L"SELECT * FROM ut_stmt_cn16", -1, db)
                 == SQLITE_OK);
monolite cols = stmt.column_names16();
UNIT_TEST_CHECK(cols.size() == 3);
UNIT_TEST_ASSERT(cols.get(0) != 0);
UNIT_TEST_CHECK(wcscmp((const wchar_t*)cols.get(0), L"id") == 0);
UNIT_TEST_ASSERT(cols.get(1) != 0);
UNIT_TEST_CHECK(wcscmp((const wchar_t*)cols.get(1), L"content") == 0);
UNIT_TEST_ASSERT(cols.get(2) != 0);
UNIT_TEST_CHECK(wcscmp((const wchar_t*)cols.get(2), L"title") == 0);
UNIT_TEST_END(statement_column_names16)
#endif

monolite statement::column_names16()
{
    monolite rVal;
    int cols = column_count();
    if (cols < 1)
        return rVal;
    size_t nBytes = 0;
    for (int i=0; i<cols; ++i)
    {
        const short* str = (const short*) column_name16(i);
        size_t len = 0;
        if (str){
            const short* tmp = str; do len += 2; while (*tmp++);
        }
        nBytes += len;
    }
    if (!rVal.reserve(cols, nBytes))
        return rVal;
    for (int i=0; i<cols; ++i)
    {
        const short* str = (const short*) column_name16(i);
        size_t len = 0;
        if (str){
            const short* tmp = str; do len += 2; while (*tmp++);
        }
        rVal.append(str, len);
    }
    return rVal;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(statement_column_values)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
transaction tr;
statement stmt;
UNIT_TEST_ASSERT(tr.begin(db) == SQLITE_OK);
const char* qstr = "CREATE TABLE test_insert("
    "id INTEGER PRIMARY KEY, "
    "param TEXT NOT NULL, "
    "value TEXT NOT NULL"
    ")";
UNIT_TEST_ASSERT(db.exec(qstr) == SQLITE_OK);
qstr = "INSERT INTO test_insert('id','param', 'value') VALUES(?,?,?)";
UNIT_TEST_ASSERT(stmt.prepare(qstr, -1, db) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.bind_int(1, 0) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.bind_text(2, "param-1", -1, 0) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.bind_text(3, "value-1", -1, 0) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.step() == SQLITE_DONE);
UNIT_TEST_ASSERT(stmt.prepare("SELECT * FROM test_insert") == SQLITE_OK);
monolite cols = stmt.column_names();
UNIT_TEST_ASSERT(cols.size() == 3);
UNIT_TEST_ASSERT(!strcmp("id", (const char*)cols.get(0)));
UNIT_TEST_ASSERT(!strcmp("param", (const char*)cols.get(1)));
UNIT_TEST_ASSERT(!strcmp("value", (const char*)cols.get(2)));
UNIT_TEST_ASSERT(stmt.step() == SQLITE_ROW);
cols = stmt.column_values();
UNIT_TEST_ASSERT(cols.size() == 3);
UNIT_TEST_ASSERT(!strcmp("0", (const char*)cols.get(0)));
UNIT_TEST_ASSERT(!strcmp("param-1", (const char*)cols.get(1)));
UNIT_TEST_ASSERT(!strcmp("value-1", (const char*)cols.get(2)));
UNIT_TEST_END(statement_column_values)
#endif

monolite statement::column_values()
{
    monolite rVal;
    int cols = column_count();
    if (cols < 1)
        return rVal;

    size_t nBytes = 0;

    const unsigned char* col_strs[cols];
    int col_lens[cols];

    for (int i=0; i<cols; ++i)
    {
        col_strs[i] = column_text(i);
        col_lens[i] = column_bytes(i);
        if (col_strs[i])
            nBytes += col_lens[i] + 1;
    }

    if (!rVal.reserve(cols, nBytes))
        return rVal;

    for (int i=0; i<cols; i++)
    {
        if (col_strs[i])
            rVal.append(col_strs[i], col_lens[i]+1);
        else
            rVal.append(0, 0);
    }
    return rVal;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(statement_column_values16)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
transaction tr;
statement stmt;
UNIT_TEST_ASSERT(tr.begin(db) == SQLITE_OK);
const wchar_t* qstr = L"CREATE TABLE test_insert("
    L"id INTEGER PRIMARY KEY, "
    L"param TEXT NOT NULL, "
    L"value TEXT NOT NULL"
    L")";
UNIT_TEST_ASSERT(stmt.prepare16(qstr, -1, db) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.step() == SQLITE_DONE);
qstr = L"INSERT INTO test_insert('id','param', 'value') VALUES(?,?,?)";
UNIT_TEST_ASSERT(stmt.prepare16(qstr) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.bind_int(1, 0) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.bind_text16(2, L"param-1", -1, 0) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.bind_text16(3, L"value-1", -1, 0) == SQLITE_OK);
UNIT_TEST_ASSERT(stmt.step() == SQLITE_DONE);
UNIT_TEST_ASSERT(stmt.prepare16(L"SELECT * FROM test_insert") == SQLITE_OK);
monolite cols = stmt.column_names16();
UNIT_TEST_ASSERT(cols.size() == 3);
UNIT_TEST_ASSERT(!wcscmp(L"id", (const wchar_t*)cols.get(0)));
UNIT_TEST_ASSERT(!wcscmp(L"param", (const wchar_t*)cols.get(1)));
UNIT_TEST_ASSERT(!wcscmp(L"value", (const wchar_t*)cols.get(2)));
UNIT_TEST_ASSERT(stmt.step() == SQLITE_ROW);
cols = stmt.column_values16();
UNIT_TEST_ASSERT(cols.size() == 3);
UNIT_TEST_ASSERT(!wcscmp(L"0", (const wchar_t*)cols.get(0)));
UNIT_TEST_ASSERT(!wcscmp(L"param-1", (const wchar_t*)cols.get(1)));
UNIT_TEST_ASSERT(!wcscmp(L"value-1", (const wchar_t*)cols.get(2)));
UNIT_TEST_END(statement_column_values16)
#endif

monolite statement::column_values16()
{
    monolite rVal;
    int cols = column_count();
    if (cols < 1)
        return rVal;
    size_t nBytes = 0;
    const void* col_strs[cols];
    int col_lens[cols];
    for (int i=0; i<cols; ++i)
    {
        col_strs[i] = column_text16(i);
        col_lens[i] = column_bytes16(i);
        if (col_strs[i])
            nBytes += col_lens[i] + 2;
    }
    if (!rVal.reserve(cols, nBytes))
        return rVal;
    for (int i=0; i<cols; i++)
    {
        if (col_strs[i])
            rVal.append(col_strs[i], col_lens[i]+2);
        else
            rVal.append(0, 0);
    }
    return rVal;
}

transaction::transaction()
{
    pConnection = 0;
}

transaction::~transaction()
{
    if (pConnection)
        rollback();
    if (pConnection) // RollBack должен обнулить pConnection!
    {
        // log... trow... etc.
    }
}

#if UNIT_TEST
UNIT_TEST_BEGIN(transaction_begin)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
{
    transaction tr;
    UNIT_TEST_CHECK(db.get_autocommit() == 1);
    UNIT_TEST_CHECK(tr.begin(db) == SQLITE_OK);
    UNIT_TEST_CHECK(db.get_autocommit() == 0);
}
UNIT_TEST_CHECK(db.get_autocommit() == 1);
UNIT_TEST_END(transaction_begin)
#endif

int transaction::begin(sqlite3* lpConn)
{
    if (!lpConn)
        return SQLITE_ERROR;
    int rc = sqlite3_exec(lpConn, "BEGIN", 0, 0, 0);
    if (rc == SQLITE_OK)
        pConnection = lpConn;
    return rc;
}

#if UNIT_TEST
static void transaction_rollback_foo(connection& db)
{
    savepoint pt;
    pt.begin(db, "transaction_rollback_foo");
    db.exec("CREATE TABLE by_throw_func(id INTEGER)");
    pt.release();
    throw 0;
}

UNIT_TEST_BEGIN(transaction_rollback)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
try {
{
    transaction tr;
    UNIT_TEST_CHECK(tr.begin(db) == SQLITE_OK);
    UNIT_TEST_CHECK(db.get_autocommit() == 0);
    UNIT_TEST_CHECK(db.exec("CREATE TABLE by_scope(id INTEGER)") == SQLITE_OK);
    transaction_rollback_foo(db);
    tr.commit();
}
}catch (int i){
}catch (...){
    UNIT_TEST_CHECK(!"Exeption!");
}
UNIT_TEST_CHECK(db.get_autocommit() == 1);
UNIT_TEST_CHECK(db.table_exists("by_scope") == false);
UNIT_TEST_CHECK(db.table_exists("by_throw_func") == false);
UNIT_TEST_END(transaction_rollback)
#endif

int transaction::rollback()
{
    if (!pConnection)
        return SQLITE_ERROR;
    int rc = sqlite3_exec(pConnection, "ROLLBACK", 0, 0, 0);
    if (rc == SQLITE_OK)
        pConnection = 0;
    return rc;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(transaction_commit)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
{
transaction tr;
UNIT_TEST_CHECK(tr.begin(db) == SQLITE_OK);
UNIT_TEST_CHECK(db.get_autocommit() == 0);
UNIT_TEST_CHECK(db.exec("CREATE TABLE commit_test(id INTEGER)") == SQLITE_OK);
UNIT_TEST_CHECK(tr.commit() == SQLITE_OK);
}
UNIT_TEST_CHECK(db.get_autocommit() == 1);
UNIT_TEST_CHECK(db.table_exists("commit_test") == true);
UNIT_TEST_END(transaction_commit)
#endif

int transaction::commit()
{
    if (!pConnection)
        return SQLITE_ERROR;
    int rc = sqlite3_exec(pConnection, "COMMIT", 0, 0, 0);
    if (rc == SQLITE_OK)
        pConnection = 0;
    return rc;
}

savepoint::savepoint()
{
    pConnection = 0;
}

savepoint::~savepoint()
{
    if (pConnection)
        rollback();
    if (pConnection) // RollBack должен обнулить pConnection!
    {
        // log... trow... etc.
    }
}

int savepoint::begin(sqlite3* lpConn, const char* szPointName)
{
    if (pConnection) // незавершенная точка?
    {
        // log... trow... etc.
        return SQLITE_ERROR;
    }
    if (!lpConn || !szPointName || !*szPointName)
    {
        // log... trow... etc.
        return SQLITE_ERROR;
    }

    char buf[sizeof(pointName)+16];
    sqlite3_snprintf(sizeof(pointName), pointName, "%s", szPointName);
    sqlite3_snprintf(sizeof(buf), buf, "SAVEPOINT %s", pointName);

    int rc = sqlite3_exec(lpConn, buf, 0, 0, 0);
    if (rc == SQLITE_OK)
        pConnection = lpConn;
    return rc;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(savepoint_rollback)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
transaction tr;
UNIT_TEST_ASSERT(tr.begin(db) == SQLITE_OK);
UNIT_TEST_CHECK(db.get_autocommit() == 0);
savepoint spt;
UNIT_TEST_ASSERT(spt.begin(db, "before") == SQLITE_OK);
UNIT_TEST_CHECK(db.exec("CREATE TABLE savepoint_rollback(id INTEGER)")
                == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("savepoint_rollback") == true);
UNIT_TEST_CHECK(spt.rollback() == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("savepoint_rollback") == false);
UNIT_TEST_CHECK(db.exec("CREATE TABLE savepoint_rollback(id INTEGER)")
                == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("savepoint_rollback") == true);
UNIT_TEST_CHECK(tr.rollback() == SQLITE_OK);
UNIT_TEST_CHECK(db.table_exists("savepoint_rollback") == false);
UNIT_TEST_CHECK(db.get_autocommit() == 1);
UNIT_TEST_END(savepoint_rollback)
#endif

int savepoint::rollback()
{
    if (!pConnection) // точка не начата?
        return SQLITE_OK;

    char buf[sizeof(pointName)+16];
    sqlite3_snprintf(sizeof(buf), buf, "ROLLBACK TO %s", pointName);
    int rc = sqlite3_exec(pConnection, buf, 0, 0, 0);
    if (rc != SQLITE_DONE)
    {
        // log... trow... etc.
    }
    return rc;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(savepoint_release)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
{
transaction tr;
UNIT_TEST_ASSERT(tr.begin(db) == SQLITE_OK);
UNIT_TEST_CHECK(db.get_autocommit() == 0);
{
    savepoint spt;
    UNIT_TEST_ASSERT(spt.begin(db, "savepoint_release") == SQLITE_OK);
    UNIT_TEST_ASSERT(db.exec("CREATE TABLE savepoint_release(id INTEGER)")
                == SQLITE_OK);
    UNIT_TEST_ASSERT(db.table_exists("savepoint_release") == true);
    UNIT_TEST_ASSERT(spt.release() == SQLITE_OK);
}
UNIT_TEST_CHECK(db.table_exists("savepoint_release") == true);
UNIT_TEST_CHECK(tr.commit() == SQLITE_OK);
UNIT_TEST_CHECK(db.get_autocommit() == 1);
}
UNIT_TEST_CHECK(db.table_exists("savepoint_release") == true);
UNIT_TEST_END(savepoint_release)
#endif

int savepoint::release()
{
    if (!pConnection) // точка не начата?
    {
        // log... trow... etc.
        return SQLITE_ERROR;
    }
    char buf[sizeof(pointName)+16];
    sqlite3_snprintf(sizeof(buf), buf, "RELEASE %s", pointName);
    int rc = sqlite3_exec(pConnection, buf, 0, 0, 0);
    if (rc != SQLITE_DONE)
    {
        // log... trow... etc.
    }
    return rc;
}

#if UNIT_TEST
UNIT_TEST_BEGIN(transactions)
connection db;
UNIT_TEST_ASSERT(db.open(":memory:") == SQLITE_OK);
transaction tr;
UNIT_TEST_ASSERT(tr.begin(db) == SQLITE_OK);
{
    savepoint first;
    UNIT_TEST_ASSERT(first.begin(db, "first_point") == SQLITE_OK);
    UNIT_TEST_ASSERT(db.exec("CREATE TABLE ut_pt_first(id INTEGER)")
                     == SQLITE_OK);
    {
        savepoint second;
        UNIT_TEST_ASSERT(second.begin(db, "second_point") == SQLITE_OK);
        UNIT_TEST_ASSERT(db.exec("CREATE TABLE ut_pt_second(id INTEGER)")
                         == SQLITE_OK);
        // second.rollback() auto
    }
    UNIT_TEST_CHECK(db.table_exists("ut_pt_second") == false);
    // first.rollback() auto
}
UNIT_TEST_CHECK(db.table_exists("ut_pt_first") == false);
UNIT_TEST_END(transactions)
#endif

} // namespace sqlite
