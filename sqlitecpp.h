/* This file is a part of sqlitecpp library.
 Copyright © 2019 Видершпан Евгений Сергеевич

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN connection WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SQLITECPP_H_INC
#define SQLITECPP_H_INC

#include <stddef.h>
#include <sqlite3.h>

#if SQLITECPP_BUILDING_DLL
    #define SQLITECPP_IMPORT __declspec (dllexport)
#else
    #if SQLITECPP_USE_DLL
        #define SQLITECPP_IMPORT __declspec (dllimport)
    #else
        #define SQLITECPP_IMPORT
    #endif // SQLITECPP_USE_DLL
#endif // SQLITECPP_BUILDING_DLL

/**
@brief   Проверка результата выполнения функций sqlite в одну строку
@details Поскольку sqlitecpp разрабатывалась с целями автоматизации рутинных
         действий (освобождение объектов/отмена транзакций/etc) то появление
         подобного макроса позволит писать функции работы с БД компактнее.
@example SQLITE_EXPECT(db.open(":memory:"), OK);
*/
#define SQLITE_EXPECT(e,v) {int r=(e); if (r!=(SQLITE_##v)) return r;}


namespace sqlite
{

/**
@brief   Монолитное хранилище обьектов (строк к примеру)
@details sqlite::monolite был разработан специально для
         загрузки текстовых данных из таблиц в память.
         работает даже быстрее чем sqlite3_get_table()
         и при этом может работать с unicode данными.
         Главное отличие от других методов - одно выделение
         памяти в reserve() на один Step() независимо от
         колличества столбцов в строке.
         Немаловажно то, что sqlite::monolite это по сути
         аналог shared_ptr с подсчётом ссылок, поэтому передача
         sqlite::monolite по значению - только приветствуется,
         т.к. это по сути то же самое как передать указатель
         и увеличить счетчик ссылок на 1.
*/
class SQLITECPP_IMPORT monolite
{
public:
    monolite():shared(0){};
    ~monolite(){detach();};
    monolite(const monolite& other):shared(0){attach(other);};
    monolite& operator=(const monolite& other){
        attach(other);
        return *this;
    };
    /// detach() + привязка к other
    void attach(const monolite& other){
        detach();
        shared = other.shared;
        if (shared)
            ++(shared->refs);
    };
    /// отсоединение от блока памяти
    void detach(){
        if (!shared) return;
        if (--(shared->refs) == 0)
            delete[] ((char*)shared);
        shared = 0;
    };
    /// detach() + рапределение нового блока памяти.
    bool reserve(size_t count, size_t size);
    /// Добавление нового обьекта в блок памяти.
    /// Размер должен учитывать символ окончания строки (если хранить строки без
    /// нулей, то get(0) вернёт ссылку на строку содержащую все добавленные
    /// строки, что позволяет использовать monolite для конкатенации строк.
    /// monolite гарантирует наличие нулевого символа размером в 2 байта в конце
    /// блока памяти (добавляются 2 символа при reserve к size), но во избежание
    /// появления "мусора" на конце обьединёных строк нужно либо заполнять
    /// с помощью append() ВСЕ запрошенные в reserve() байты, либо последнюю
    /// строку добавить вместе с нулём.
    bool append(const void* pdata, size_t size);
    /// Возвращает ссылку на обьект по индексу + его размер в lpSize
    const void* get(size_t nItem, size_t* lpSize=0) const;
    /// Возвращает копию своего состояния с оптимальным размером блока памяти.
    /// Используется в случае когда один обьект monolite заранее рапределёный с
    /// запасом по строкам и размеру блока данных служит приёмником заранее
    /// неизвестного числа строк и их размеров, deflate() возвращает копию
    /// данных с колличеством уже добавленных строк и блоком памяти в их размер.
    monolite deflate();
    /// создаёт копию текущего состояния с удвоенным размером от текущего
    monolite inflate(bool onlyData=false);
    /// возвращает колличество обектов уже записаных в блок памяти
    int size() const {
        if (!shared)
            return 0;
        return shared->saved;
    };
    /// возвращает колличество байт распределёных под данные
    size_t reserved() const;
    /// Забывает о ранее добавленых строках.
    /// Во избежание порчи разделяемых данных оказывает действие только на
    /// обьекты с 1 действующей ссылкой.
    bool reset(){
        if (!shared)
            return false;
        if (shared->refs != 1)
            return false;
        shared->saved = 0;
        return true;
    };
private:
    struct shared_data {
        size_t refs;
        size_t size;
        size_t count;
        size_t saved;
        size_t offsets[1];
    };
    shared_data* shared;
};

/**
@brief   указатель на функцию-деалокатор памяти или спец-значение
*/
typedef void (*destructor)(void*);

/**
@brief   Класс-пустышка запрещающий копирование своих наследников
*/
class SQLITECPP_IMPORT noncopyable {
protected:
    noncopyable(){}
    ~noncopyable(){}
private:
    noncopyable (const noncopyable&);
    noncopyable& operator=(const noncopyable&);
};

/**
@brief   Соединение с базой данных
*/
class SQLITECPP_IMPORT connection: noncopyable {
public:
    connection();
    virtual ~connection();
    virtual int open(const char *filename);
    virtual int open16(const void *filename);
    virtual int close();
    virtual int exec(const char *szSql);
    virtual int get_autocommit();
    virtual bool table_exists(const char *szname);

    sqlite3* handle() const { return pConnection; };
    operator sqlite3* () const { return pConnection; };
private:
    sqlite3* pConnection;
};

/**
@brief   SQL компилятор
*/
class SQLITECPP_IMPORT statement: noncopyable {
public:
    statement();
    virtual ~statement();

    virtual int prepare(const char *szSql, int nBytes=-1, sqlite3* lpConn=0);
    virtual int prepare16(const void *szSql, int nBytes=-1, sqlite3* lpConn=0);
    virtual int reset();
    virtual int finalize();
    virtual int step();

    virtual int bind_blob(int nCol, const void* pValue, int nBytes, destructor);
    virtual int bind_double(int nCol, double value);
    virtual int bind_int(int nCol, int value);
    virtual int bind_int64(int nCol, sqlite_int64 value);
    virtual int bind_null(int nCol);
    virtual int bind_parameter_count();
    virtual int bind_parameter_index(const char* szName);
    virtual const char* bind_parameter_name(int nCol);
    virtual int bind_text(int nCol, const char* value, int nBytes, destructor);
    virtual int bind_text16(int nCol, const void* value, int nBytes, destructor);
    virtual int bind_value(int nCol, const sqlite3_value* value);
    virtual int clear_bindings();

    virtual const char* column_database_name(int nCol);
    virtual const char* column_decltype(int  nCol);
    virtual const char* column_name(int nCol);
    virtual const char* column_origin_name(int nCol);
    virtual const char* column_table_name(int nCol);
    virtual const unsigned char* column_text(int nCol);
    virtual const void* column_blob(int nCol);
    virtual const void* column_database_name16(int nCol);
    virtual const void* column_decltype16(int nCol);
    virtual const void* column_name16(int nCol);
    virtual const void* column_origin_name16(int nCol);
    virtual const void* column_table_name16(int nCol);
    virtual const void* column_text16(int nCol);
    virtual double column_double(int nCol);
    virtual int column_bytes(int nCol);
    virtual int column_bytes16(int nCol);
    virtual int column_count();
    virtual int column_int(int nCol);
    virtual int column_type(int nCol);
    virtual sqlite_int64 column_int64(int nCol);
    virtual sqlite3_value* column_value(int nCol);

    /// Возвращает список utf-8 строк с именами заголовков
    virtual monolite column_names();
    /// Возвращает список unicode строк с именами заголовков
    virtual monolite column_names16();
    /// Возвращает список utf-8 строк со значениями столбцов
    /// доступен к использованию только если последний вызов
    /// step() вернул код SQLITE_ROW.
    /// при использовании могут стать невалидными указатели
    /// на данные запроса полученные ранее от column_text()
    /// и их аналогов (особенность самой sqlite3), поэтому
    /// рекомендуется сразу после step() делать column_values()
    /// (если планируется сохранять данные в виде monolite)
    /// в этом случае у вас просто не будет указателей
    /// которые могли бы испортиться.
    virtual monolite column_values();
    /// То же что и column_values() но в виде unicode
    virtual monolite column_values16();

    sqlite3* db_handle();
    sqlite3_stmt* handle() { return pStatement; };
    operator sqlite3_stmt* () const { return pStatement; };

private:
    sqlite3_stmt* pStatement;
};

/**
@brief   SQL транзакция с rollBack() в деструкторе, если небыло commit()
*/
class SQLITECPP_IMPORT transaction: noncopyable {
public:
    transaction();
    virtual ~transaction();
    virtual int begin(sqlite3* lpConn);
    virtual int rollback();
    virtual int commit();
private:
    sqlite3* pConnection;
};

/**
@brief   SQL savepoint с rollback() в деструкторе, если небыло release()
*/
class SQLITECPP_IMPORT savepoint: noncopyable {
public:
    savepoint();
    virtual ~savepoint();
    virtual int begin(sqlite3* lpConn, const char* szPointName);
    virtual int rollback();
    virtual int release();
private:
    sqlite3* pConnection;
    char pointName[32];
};

} // namespace sqlite
#endif // SQLITECPP_H_INC
